<?hh //strict
namespace hello
{
	use nuclio\plugin\application\application\Application;
	
	class Hello extends Application
	{
		public function run():void
		{
			print <strong>Hello Nuclio!</strong>;
		}
	}
}